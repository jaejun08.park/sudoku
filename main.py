from tkinter import *
# window
window = Tk()
window.title("sudoku!")
window.config()

# canvas
canvas = Canvas(width=600, height=600)
back_ground = PhotoImage(file="sudoku-blankgrid.png")
canvas.create_image(300, 300, image=back_ground)
canvas.grid(row=0, column=0, columnspan=8, rowspan=8)
# 657,633
# entry

default_x = 90
default_y = 100
at_row = 0
at_column = 0

def making_entry():
    global default_y
    global default_x
    global at_row
    global at_column
    while at_row < 9:
        entry = Entry(width=5)
        entry.place(x=default_x, y=default_y)
        default_x += 47
        at_column += 1
        if at_column % 3 == 0:
            default_x += 6
        if at_column == 9:
            at_row += 1
            default_y += 47
            default_x = 90
            at_column = 0


making_entry()

window.mainloop()
